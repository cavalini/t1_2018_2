#include "sequencial.h"

/** Calcula a soma das posicoes de rainhas que nao se atacam em tabuleiros de dim x dim 
  * Cada posicao do tabuleiro é uma potencia de dois (2^0, 2^1, ...), da esquerda para direita,
  * de cima para baixo.
  *
  * Ex: 3x3 
  * X 0 0    0 X 0   
  * 0 0 X    0 0 0   ...
  * 0 0 0    X 0 0
  *
  * 2^0 + 2^5 = 1 + 32 = 33 
  * 2^1 + 2^6 = 2 + 64 = 66
  * ...
  * Os valores de todas as possibilidades devem ser somados e retornados pela funcao
  * Caso os parametros sejam invalidos, retorne 0
  */

  
unsigned long long nqueens(int dim, int queens) {

	int i, j;
	unsigned long long fat;
	
	argumentos *args;

	args = (argumentos*) malloc(sizeof(argumentos));
	args->tabuleiro = (int*) malloc(sizeof(int[dim][dim]));
	memset(args->tabuleiro, 0, dim*dim*sizeof(int));
	args->q = queens;
	args->d = dim;
	
	if (args->d < args->q)
	{
		return 0;
	}
	soma = 0;
	args->q--;
	

		for(i = 0; i < dim; i++)
		{
			for(j = 0; j < dim; j++)
			{	
				*((args->tabuleiro+i*dim)+j) = 1;
				alocar((void*) args);
				memset(args->tabuleiro, 0, dim*dim*sizeof(int));
			}
		}
	
	
	args->q++;
	fat = fatorial(args->q);
	return soma/fat;
}

void *alocar( void *arguments)
{
	int i, j;
	argumentos *args = (argumentos*) malloc(sizeof(argumentos));
	
	//args->tabuleiro = (int*) malloc(sizeof(int[arguments->d][arguments->d]));
	
	memmove(args, (argumentos *)arguments, sizeof(argumentos));
	
	if(args->q)
	{
		for (i = 0; i < args->d; i++)
		{
			for (j = 0; j < args->d; j++)
			{	
				if (verificar(args->tabuleiro, args->d, i, j))
				{
					args->q--;
					*(((args->tabuleiro)+i*args->d)+j) = 1;
					alocar((void*) args);
					*(((args->tabuleiro)+i*args->d)+j) = 0;
					args->q++;
				}
			}
		}
	}
	else
	{
		imprimir(args->tabuleiro, args->d);
	}
	return NULL;
}

unsigned long long elevar(unsigned long long base, int expoente)
{
	unsigned long long resultado = 1ULL;
	
	while(expoente)
	{
		if (expoente & 1)
		{
			resultado *= base;
		}
		expoente >>= 1;
		base *= base;
	}
	return resultado;
}

unsigned long long fatorial (int q)
{
	unsigned long long fat;
	
	for(fat = 1; q > 1; q--)
		fat = fat*q;
		
	return fat;
}

void imprimir(int *ptr, int dim) {

	int i, j;
	int potencia = 0;
	
	//printf("Tabuleiro:\n");
	for (i = 0; i < dim; i++)
	{
		//printf("| ");
		for (j = 0; j < dim; j++)
		{
			//*((ptr+i*dim)+j) = i+j;
			if (*((ptr+i*dim)+j) == 1)
			{
				pthread_mutex_lock(&lock);
				soma += elevar(2ULL, potencia);
				pthread_mutex_unlock(&lock);
			}
			//printf("%d  ", *((ptr+i*dim)+j));
			potencia++;
		}
		//printf("|\n");
	}
}

// verifica se no alcance da rainha possui uma outra rainha
int verificar(int *ptr, int dim, int i, int j)
{
	//*((ptr+i*dim)+j)
	
	int k;

	for (k = 0; k < dim; k++)
	{
		if (*((ptr+k*dim)+j))
		{
			return 0; //*((ptr+k*dim)+j) = 1;
		}
		if (*((ptr+i*dim)+k))//tabuleiro[i][k])
		{
			return 0; //*((ptr+i*dim)+k) = 1;
		}
		if ((i+k < dim) && (j+k < dim))
		{
			if (*((ptr+(i+k)*dim)+(j+k)))//tabuleiro[i+k][j+k])
			{
				return 0; //*((ptr+(i+k)*dim)+(j+k)) = 1;
			}
		}
		if ((i+k < dim) && (j-k >= 0))
		{
			if (*((ptr+(i+k)*dim)+(j-k)))//tabuleiro[i+k][j-k])
			{
				return 0; //*((ptr+(i+k)*dim)+(j-k)) = 1;
			}
		}
		if ((i-k >= 0) && (j-k >= 0))
		{
			if (*((ptr+(i-k)*dim)+(j-k)))//tabuleiro[i-k][j-k])
			{
				return 0; //*((ptr+(i-k)*dim)+(j-k)) = 1;
			}
		}
		if ((i-k >= 0) && (j+k < dim))
		{
			if (*((ptr+(i-k)*dim)+(j+k)))//tabuleiro[i-k][j+k])
			{
				return 0; //*((ptr+(i-k)*dim)+(j+k)) = 1;
			}
		}
	}
	return 1;	
}

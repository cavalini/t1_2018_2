#ifndef _NQUEENST_C_
#define _NQUEENST_C_

#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include "nqueenst.h"
#include <stdlib.h>

	
	
unsigned long long nqueens(int dim, int queens) {

	int i, j, t = 1, t2 = 1, t3 = 1, t4 = 1;
	unsigned long long fat;
	
	if (dim < queens)
	{
		return 0;
	}

	pthread_t thread[NTHREADS];
	
	if (pthread_mutex_init(&lock, NULL))
	{
		printf("mutex init falhou\n");
		return 1;
	}
	
	argumentos *args, *args2, *args3, *args4;

	args = (argumentos*) malloc(sizeof(argumentos));
	args2 = (argumentos*) malloc(sizeof(argumentos));
	args3 = (argumentos*) malloc(sizeof(argumentos));
	args4 = (argumentos*) malloc(sizeof(argumentos));
	args->tabuleiro = (int*) malloc(sizeof(int[dim][dim]));
	args2->tabuleiro = (int*) malloc(sizeof(int[dim][dim]));
	args3->tabuleiro = (int*) malloc(sizeof(int[dim][dim]));
	args4->tabuleiro = (int*) malloc(sizeof(int[dim][dim]));
	memset(args->tabuleiro, 0, dim*dim*sizeof(int));
	memset(args2->tabuleiro, 0, dim*dim*sizeof(int));
	memset(args3->tabuleiro, 0, dim*dim*sizeof(int));
	memset(args4->tabuleiro, 0, dim*dim*sizeof(int));
	args->q = queens;
	args->d = dim;
	args2->q = queens;
	args2->d = dim;
	args3->q = queens;
	args3->d = dim;
	args4->q = queens;
	args4->d = dim;
	
	soma = 0;
	args->q--;
	args2->q--;
	args3->q--;
	args4->q--;

		for(i = 0; i < dim; i++)
		{
			for(j = 0; j < dim; j++)
			{	
				
				*((args->tabuleiro+i*dim)+j) = 1;
				t = pthread_create(&thread[0], NULL, alocar, (void*)args);
				j++;
				if(j < dim)
				{
					*((args2->tabuleiro+i*dim)+j) = 1;
					t2 = pthread_create(&thread[1], NULL, alocar, (void*)args2);
					j++;
				}
				if(j < dim)
				{
					*((args3->tabuleiro+i*dim)+j) = 1;
					t3 = pthread_create(&thread[2], NULL, alocar, (void*)args3);
					j++;
				}
				if(j < dim)
				{
					*((args4->tabuleiro+i*dim)+j) = 1;
					t4 = pthread_create(&thread[3], NULL, alocar, (void*)args4);
				}
				
				if (!t)
				{
					if(!pthread_join(thread[0], NULL))
					{
						//printf("Thread 1 Join com sucesso\n");
						//getchar();
					}
					memset(args->tabuleiro, 0, dim*dim*sizeof(int));
				}
				if (!t2)
				{
					while(!pthread_join(thread[1], NULL))
					{
						//printf("Thread 2 Join com sucesso\n");
						//getchar();
					}
					memset(args2->tabuleiro, 0, dim*dim*sizeof(int));
				}
				if (!t3)
				{
					//pthread_join(thread[2], NULL);
					while(!pthread_join(thread[2], NULL))
					{
						//printf("Thread 3 Join com sucesso\n");
						//getchar();
					}
					memset(args3->tabuleiro, 0, dim*dim*sizeof(int));
				}
				if (!t4)
				{
					while(!pthread_join(thread[3], NULL))
					{
						//printf("Thread 4 Join com sucesso\n");
						//getchar();
					}
					memset(args4->tabuleiro, 0, dim*dim*sizeof(int));
				}
			}
		}	
	
	
	pthread_mutex_destroy(&lock);
	

	fat = fatorial(queens);
	return soma/fat;
}

void *alocar( void *arguments)
{
	int i, j;
	argumentos *args = (argumentos*) malloc(sizeof(argumentos));
	
	//args->tabuleiro = (int*) malloc(sizeof(int[arguments->d][arguments->d]));
	
	memmove(args, (argumentos *)arguments, sizeof(argumentos));
	
	if(args->q)
	{
		for (i = 0; i < args->d; i++)
		{
			for (j = 0; j < args->d; j++)
			{	
				if (verificar(args->tabuleiro, args->d, i, j))
				{
					args->q--;
					*(((args->tabuleiro)+i*args->d)+j) = 1;
					alocar((void*) args);
					*(((args->tabuleiro)+i*args->d)+j) = 0;
					args->q++;
				}
			}
		}
	}
	else
	{
		imprimir(args->tabuleiro, args->d);
	}
	return NULL;
}

unsigned long long elevar(unsigned long long base, int expoente)
{
	unsigned long long resultado = 1ULL;
	
	while(expoente)
	{
		if (expoente & 1)
		{
			resultado *= base;
		}
		expoente >>= 1;
		base *= base;
	}
	return resultado;
}

unsigned long long fatorial (int q)
{
	unsigned long long fat;
	
	for(fat = 1; q > 1; q--)
		fat = fat*q;
		
	return fat;
}

void imprimir(int *ptr, int dim) {

	int i, j;
	int potencia = 0;
	
	//printf("Tabuleiro:\n");
	for (i = 0; i < dim; i++)
	{
		//printf("| ");
		for (j = 0; j < dim; j++)
		{
			//*((ptr+i*dim)+j) = i+j;
			if (*((ptr+i*dim)+j) == 1)
			{
				pthread_mutex_lock(&lock);
				soma += elevar(2ULL, potencia);
				pthread_mutex_unlock(&lock);
			}
			//printf("%d  ", *((ptr+i*dim)+j));
			potencia++;
		}
		//printf("|\n");
	}
}

// verifica se no alcance da rainha possui uma outra rainha
int verificar(int *ptr, int dim, int i, int j)
{
	//*((ptr+i*dim)+j)
	
	int k;

	for (k = 0; k < dim; k++)
	{
		if (*((ptr+k*dim)+j))
		{
			return 0; //*((ptr+k*dim)+j) = 1;
		}
		if (*((ptr+i*dim)+k))//tabuleiro[i][k])
		{
			return 0; //*((ptr+i*dim)+k) = 1;
		}
		if ((i+k < dim) && (j+k < dim))
		{
			if (*((ptr+(i+k)*dim)+(j+k)))//tabuleiro[i+k][j+k])
			{
				return 0; //*((ptr+(i+k)*dim)+(j+k)) = 1;
			}
		}
		if ((i+k < dim) && (j-k >= 0))
		{
			if (*((ptr+(i+k)*dim)+(j-k)))//tabuleiro[i+k][j-k])
			{
				return 0; //*((ptr+(i+k)*dim)+(j-k)) = 1;
			}
		}
		if ((i-k >= 0) && (j-k >= 0))
		{
			if (*((ptr+(i-k)*dim)+(j-k)))//tabuleiro[i-k][j-k])
			{
				return 0; //*((ptr+(i-k)*dim)+(j-k)) = 1;
			}
		}
		if ((i-k >= 0) && (j+k < dim))
		{
			if (*((ptr+(i-k)*dim)+(j+k)))//tabuleiro[i-k][j+k])
			{
				return 0; //*((ptr+(i-k)*dim)+(j+k)) = 1;
			}
		}
	}
	return 1;	
}
#endif
